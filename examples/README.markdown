# Reading `mpf` files

The ipython notebook `mpf.ipynb` gives a few examples of how `mpf` files can be
read in python. Note that in order to run this example, you should have the
following modules installed:

- `miopyc` (can be installed with `python setup.py install`)
- `numpy`
- `matplotlib`
- `ipython` (with the notebook interface)

In our experience, the easiest way of installing all packages is to install the
[anaconda python distribution version 3.4 or newer](https://store.continuum.io/cshop/anaconda),
which contains all the dependencies except `miopyc` itself.

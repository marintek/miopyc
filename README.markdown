# MIOPYC: Marintek IO PYthon paCkage

At MARINTEK, several in-house result file formats have been created over the
years. This python module is intended to supply a pythonic interface to reading
these files. `miopyc` is used internally at MARINTEK for software testing.



## Dependencies

In order to use the software within, you will need:

- a `python` interpreter, both version 2 (i.e. 2.7) and 3 are supported
- the `numpy` module for numerical array support in python

Optionally, to run the examples, you will need:

- an `ipython notebook`
- the `matplotlib` module for plotting

The easiest way to get the above dependencies is to install the
[anaconda python distribution][anaconda], which is available for all major
operating systems, and contains all of the above.


[anaconda]: https://store.continuum.io/cshop/anaconda/ "anaconda python distribution"



## Running the examples

Go to the directory `examples` and run `ipython notebook`; this should open up
the ipython notebook interface. Select the notebook `mpf` to review the
examples for reading files in Matrix Plot Format. An introduction to ipython
notebook can be found [here][ipython-tutorial].

[ipython-tutorial]: http://ipython.org/ipython-doc/3/interactive/tutorial.html "ipython tutorial"



## Installation

Install the `miopyc` package by cloning/downloading the source tree and
running the following command in the root directory:

```bash
# Global installation
python setup.py install
# Per-user installation (does not require administrative privileges)
python setup.py install --user
```

Then, the `miopyc` module is available in your global python installation:

```python
import miopyc
```

## Description of the command line program `mpf2db'

```
usage: mpf2db [-h] [-k] [-p PREFIX] [-v] [-w WARN] mpffile [dbfile]

'mpf2db' moves data (key/value-pairs) from a mpf-file to a database file.
The keys used in the database file are derived from the matrix names and
the legends applied in the mpf-file. The suffix of the database file name
determines the database type.

Three database types are supported (if available):
a) Native Python shelve: mpf2db input.mpf output.db
b) HDF5:                 mpf2db input.mpf output.h5
c) Berkeley DB:          mpf2db input.mpf output.ndb

Example:
1. Create a Python shelve database:
     $ mpf2db input.mpf output.db
2. How to obtain data from a shelve database using Python:
     import shelve
     db = shelve.open('output.db')
     for key in db.keys():
	 print(key, ":", db[key])
In case of several matrices with the same name within the mpf-file,
the content of the last matrix is saved.


positional arguments:
  mpffile               input, mpf-file
  dbfile                output, database file name
			The keys will be written if left out, similar to the option '-k'.

optional arguments:
  -h, --help            show this help message and exit
  -k, --key-list        show keys only
  -p PREFIX, --prefix PREFIX
			prefix for keys
  -v, --version         show program's version number and exit
  -w WARN, --warn WARN  "Examples: '-w error', '-w always', '-w once' and '-w ignore'.
			This is used to change the program behaviour when parsing the mpf-file.
			The default is to process as much data as possible, '-w once'.
			By specifying '-w error', an immediate stop is caused and no database
			file is written in case of anomalies.
```

You may use `mpf2db` within your own Python program, example:
```
import sys
import miopyc
import shelve

mpf    = "in.mpf"                # mpf: matrixplotfile
dbfile = "out.db"                # db:  a shelve database

miopyc.mpf2db( [ mpf, dbfile ] ) # move data

# list the keys within the shelve database
db = shelve.open(dbfile)
keys=sorted(list(db.keys()))
for key in keys:
    sys.stdout.write("%s\n" %(key))

# show both key and value for the last entry
lastkey=keys[-1]
lastvalue=db[lastkey]
sys.stdout.write("key:   %s\n"%(lastkey))
sys.stdout.write("value: %s\n"%(lastvalue))

db.close()

```



# License

`miopyc` is made available under the BSD 2-clause license. See
[LICENSE.markdown][license] for further details.

[license]: LICENSE.markdown

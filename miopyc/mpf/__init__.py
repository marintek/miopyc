from .mpffile import MpfMatrix, MpfFile
from .mpffile import DuplicateKeyError

from .mpf2db  import main as mpf2db

#!/usr/bin/env python -u

import os
import sys
import argparse

import re
import warnings

import shelve
import collections


try:
    import h5py
    has_h5py = True
except ImportError:
    has_h5py = False

try:
    import ndb
    has_ndb = True
except ImportError:
    has_ndb = False

import miopyc


def my_formatwarning(message, category, filename, lineno, line):
    """
    This function provides a custom format of warning messages from the
    warnings module.
    """
    if filename.endswith('mpf2db.py'):
        filename = 'mpf2db'
    return "%s:%s:%s\n" % (filename, category.__name__, message)
warnings.formatwarning = my_formatwarning


def parseArguments(argv):
    """
    Parsing of command line arguments.
    """
    DESCRIPTION = '''
'mpf2db' moves data (key/value-pairs) from a mpf-file to a database file.
The keys used in the database file are derived from the matrix names and
the legends applied in the mpf-file. The suffix of the database file name
determines the database type.

Three database types are supported (if available):
a) Native Python shelve: mpf2db input.mpf output.db
b) HDF5:                 mpf2db input.mpf output.h5
c) Berkeley DB:          mpf2db input.mpf output.ndb

Example:
1. Create a Python shelve database:
     $ mpf2db input.mpf output.db
2. How to obtain data from a shelve database using Python:
     import shelve
     db = shelve.open('output.db')
     for key in db.keys():
         print key, ":", db[key]
In case of several matrices with the same name within the mpf-file,
the content of the last matrix is saved.
    '''
    parser = argparse.ArgumentParser(description=DESCRIPTION,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('mpffile', type=str,
                        help="input, mpf-file")
    parser.add_argument('dbfile', type=str, nargs="?", default=None,
                        help="""output, database file name
The keys will be written if left out, similar to the option '-k'.""")
    parser.add_argument('-k', '--key-list', action="store_true",
                        help='show suggested keys only')
    parser.add_argument('-p', '--prefix', type=str, default=None,
                        help="add prefix for keys")
    parser.add_argument('-s', '--substitute', action="append", default=[],
                        help='''the suggested keys as listed by option '-k' may be changed by
text substitution. The change is performed by replacing the
non-empty matching part of REGEX with THAT in all keys.
Several replacements can be specified, they are applied in sequence.
The replacement specification must be written as follows:
'-s/REGEX/THAT/' or --substitute="/REGEX/that/".''')
    parser.add_argument('-v', '--version', action="version",
                        version='%(prog)s version 0.21')
    parser.add_argument('-w', '--warn', type=str, default="once",
                        help=""""Examples: '-w error', '-w always', '-w once' and '-w ignore'.
This is used to change the program behaviour when parsing the mpf-file.
The default is to process as much data as possible, '-w once'.
By specifying '-w error', an immediate stop is caused and no database
file is written in case of anomalies.""")
    arguments = parser.parse_args(argv)
    retval=0
    tmp=[]
    for x in arguments.substitute:
        if len(x)<4 or x[0]!=x[-1] or x[1:-1].find(x[0])<1:
            msg = "error: the substitution argument must look like '/REGEX/replacement/', got: %s\n"%(x)
            sys.stderr.write(msg)
            retval=1
        else:
            regex, that = x[1:-1].split(x[0])
            tmp.append([regex, that])
    arguments.substitute=tmp
    if not os.path.isfile(arguments.mpffile):
        msg = "error: cannot find specified mpf-file: '%s'.\n"
        sys.stderr.write(msg % arguments.mpffile)
        retval=1
    if not os.access(arguments.mpffile, os.R_OK):
        msg = "error: cannot read specified mpf-file: '%s'.\n"
        sys.stderr.write(msg % arguments.mpffile)
        retval=1
    return arguments, retval


def main(argv=[]):
    """
    Write mpffile content to dbfile
    usage: mpf2db(['mpffile', 'dbfile', '-s/REGEX/replacement/'])
    """
    if len(argv) == 0:
        argv = sys.argv[1:]
    arguments, retval = parseArguments(argv)
    if retval==1:
        return 1

    list_of_replacements=[]
    for regex,that in arguments.substitute:
        try:
            list_of_replacements.append([re.compile(regex), regex, that])
        except:
            sys.stderr.error("error: regex compilation failure, regex='%s'\n"%(regex))
            return 1
    #
    warnings.simplefilter(arguments.warn)
    if arguments.warn == "error":
        ignore_duplicates = False
    else:
        ignore_duplicates = True

    infile = arguments.mpffile

    try:
        mf = miopyc.MpfFile(infile, ignore_duplicates=ignore_duplicates)
    except miopyc.DuplicateKeyError as err:
        sys.stderr.write("error: %s\n" % err)
        return 1

    if arguments.key_list or arguments.dbfile is None or arguments.dbfile == '':
        dbfile = ''
        db = None
        keysonly = True
    else:
        dbfile = arguments.dbfile
        if dbfile.endswith('h5') or dbfile.endswith('hdf5'):
            if has_h5py:
                db = h5py.File(dbfile,'w')
                keysonly = False
            else:
                sys.stderr.write("error: missing module: h5py\n")
                return 1
        elif dbfile.endswith('ndb'):
            if has_ndb:
                db = ndb.Ndb()
                db.open(dbfile)
                keysonly = False
            else:
                sys.stderr.write("error: missing module: ndb\n")
                return 1
        else:
            db = shelve.open(dbfile)
            keysonly = False

    if arguments.prefix is None:
        prefix = ''
    else:
        prefix = arguments.prefix + '-'

    D, allkeys = {}, []
    matrixnames = mf.keys()

    for name in matrixnames:
        D = {}
        m = mf[name]
        key = name + '-' + m.xlegend
        # remove trailing spaces in m.xlegend
        key = re.sub('[ \t]+$', '', key)
        # replace and minimize special characters
        key = re.sub('[(),/ ]+', '-', key)
        key = key.replace('--', '-')
        key = key.replace('--', '-')
        key = key.replace('--', '-')
        key = prefix + key
        value = m.xvalues
        D[key] = value
        allkeys.append(key)

        if 'TITLE' in m.attrs:
            key, value = name+'-'+'TITLE', m.attrs['TITLE']
            key = re.sub('[(),/ ]+', '-', key)
            key = key.replace('--', '-')
            key = key.replace('--', '-')
            key = key.replace('--', '-')
            key = prefix + key
            D[key] = value
            allkeys.append(key)
            if "Eigenvector" in key and value.startswith("Eigenvector"):
                # extract additional information for Riflex dynmod
                key = key.replace('-TITLE', '-modeno')
                value = value.split()[2].replace(',', '')
                D[key] = float(value)
                allkeys.append(key)
        if 'SUBTITLE' in m.attrs:
            key, value = name+'-'+'SUBTITLE', m.attrs['SUBTITLE']
            key = re.sub('[(),/ ]+', '-', key)
            key = key.replace('--', '-')
            key = key.replace('--', '-')
            key = key.replace('--', '-')
            key = prefix + key
            D[key] = value
            allkeys.append(key)
            if "Eigenvector" in key and value.startswith("Frequency"):
                # extract additional information for Riflex dynmod
                key = key.replace('-SUBTITLE', '-freq')
                value = value.replace('Frequency', 'Frequency ').split()[1]
                D[key] = float(value)
                allkeys.append(key)
                key = key.replace('-freq', '-period')
                D[key] = 1./float(value)
                allkeys.append(key)

        for k in m.yvalues.keys():
            key = name + '-' + k
            # remove trailing spaces
            key = re.sub('[ \t]+$', '', key)
            # replace and minimize special characters
            key = re.sub('[(),/ ]+', '-', key)
            key = key.replace('--', '-')
            key = key.replace('--', '-')
            key = key.replace('--', '-')
            key = prefix + key
            value = m.yvalues[k]
            D[key] = value
            allkeys.append(key)

        # user can change key by replacement operations
        outkeydict={}
        for key in allkeys:
            outkey=key
            for cre,regex,that in list_of_replacements:
                outkey=cre.sub(that,outkey)                # replacements
            outkey=outkey.strip()                          # remove leading and trailing space
            if outkey=="":
                sys.stderr.write("error: empty key obtained after replacements for key:'%s'\n"%(key))
                return 1
            outkeydict[key]=outkey

        # save into the database, applying the user modified keys
        if db is not None:
            # save the content of the mpf-file
            if db.__module__ == "h5py._hl.files":
                for key in D:
                    outkey=outkeydict[key]
                    if outkey in db:
                        del db[outkey]
                    db[outkey] = D[key]
            elif db.__module__ == "ndb":
                for key in D:
                    outkey=outkeydict[key]
                    db[outkey] = D[key]
            else:
                for key in D:
                    outkey=outkeydict[key]
                    db[outkey] = D[key]

    if db is not None:
        db.close()

    if keysonly:
        D = collections.defaultdict(int)
        for key in allkeys:
            outkey=outkeydict[key]
            D[outkey] += 1
            sys.stdout.write('%s\n' % outkey)
        for key in allkeys:
            outkey=outkeydict[key]
            if D[outkey] > 1:
                msg = "warning: original key %50s is repeated as key '%s'\n" %("'%s'"%(key),outkeydict[key])
                sys.stderr.write(msg)
    return 0

if __name__ == '__main__':
    retval=main()
    sys.exit(retval)

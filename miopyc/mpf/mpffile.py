#!/usr/bin/env python
# encoding: utf-8


# python stdlib modules
import re
import sys
import collections
import warnings
# 3rd party libraries
import numpy as np


class MpfMatrix(object):
    """
    Represents the "matrix" object in a MatrixPlot Format file.
    """
    def __init__(self, legends, values, attrs):
        """
        Initialize MpfMatrix object.

        :legends:   Sequence of the legends for each column in the matrix
        :values:    ``numpy.ndarray`` containing all VALUES data for the matrix
        :attrs:     Attributes to the matrix, such as xlabel, ylabel, etc.
        """
        super(MpfMatrix, self).__init__()
        self.xlegend = legends[0]
        self.xvalues = values[:, 0]
        self.yvalues = collections.OrderedDict()
        self.attrs = attrs

        # Pack y legends and values into dict
        ny = len(legends)
        for i in range(1, ny):
            legend = legends[i]
            yvalues = values[:, i]
            self.yvalues[legend] = yvalues

    # Magic methods, mainly for the immutable sequence protocol

    def __str__(self):
        s = str(self.yvalues.keys())
        return s


class MpfFile(object):
    """
    Class for reading Matrix Plot Format (MPF) files. MPF is an ascii based
    format used by several MARINTEK programs.
    """
    # mpf format related constants
    BLANK = ''
    HEADING = 'HEADING'
    MATRIX = 'MATRIX'
    VALUES = 'VALUES'
    LEGENDS = 'LEGENDS'
    XLABEL = 'XLABEL'
    YLABEL = 'YLABEL'
    TITLE = 'TITLE'
    SUBTITLE = 'SUBTITLE'
    END = 'END'
    COMMAND_LENGTH = 10

    def __init__(self, fname, ignore_duplicates=False):
        """
        Constructor.

        :fname: The mpf file to read, either provided as a file name, file
                handle, or ``StringIO`` object.
        """
        self.fname = fname
        self.ignore_duplicates = ignore_duplicates
        self.matrices = collections.OrderedDict()

        # Open file and read contents into memory
        with open(fname, 'r') as fhandle:
            lines = fhandle.readlines()

        # Parse the HEADING of the file, if present
        firstcmd, firstval = self._getcmd(lines[0])
        i = 0
        self.heading = ""
        cmd, val = self._getcmd(lines[0])
        while cmd in (MpfFile.BLANK, MpfFile.HEADING):
            self.heading += '{0}\n'.format(val)
            i += 1
            cmd, val = self._getcmd(lines[i])
        # Remove final newline
        self.heading = self.heading[: -1]

        # Locate lines with the MATRIX command in them
        matrixlines = []
        found_end = False
        for i, line in enumerate(lines):
            cmd, val = self._getcmd(line)
            if cmd == MpfFile.MATRIX:
                matrixlines.append(i)
            elif cmd == MpfFile.END:
                # End of file identifier found
                found_end = True
                matrixlines.append(i)

        # Parse the matrices into MpfMatrix objects
        if not found_end:
            matrixlines.append(len(lines))

        for i, lineidx in enumerate(matrixlines[:-1]):
            relevantlines = lines[lineidx:matrixlines[i + 1]]
            matrixname, matrix = self._parsematrix(relevantlines, ignore_duplicates=ignore_duplicates)
            if matrixname in self.matrices:
                msg = 'Duplicate matrix "{0}" in file "{1}"'.format(
                        matrixname, fname)
                if self.ignore_duplicates:
                    warnings.warn(msg, stacklevel=2)
                else:
                    raise DuplicateKeyError(msg)
            self.matrices[matrixname] = matrix
        return

    def keys(self):
        """
        :returns: The names of all matrix plots.
        """
        return self.matrices.keys()

    # Private methods

    def _parsematrix(self, matrixlines, ignore_duplicates=False):
        """
        Parse a 'matrix' in MPF file.

        :matrixlines:   List of lines forming the matrix.
        :returns:       Matrix object.
        """
        # Name of the matrix
        firstline = matrixlines[0]
        cmd, val = self._getcmd(firstline)
        matrixname = val.strip()

        # Parse metadata and identify values
        attrs = {}
        legendpattern = re.compile(r"'.*?'")
        datalines = []
        for i, line in enumerate(matrixlines):
            cmd, val = self._getcmd(line)
            if cmd == MpfFile.LEGENDS:
                legends = legendpattern.findall(val)
                legends = [l.replace("'", "") for l in legends]
            elif cmd in (MpfFile.XLABEL, MpfFile.YLABEL, MpfFile.TITLE, MpfFile.SUBTITLE):
                attrs[cmd] = val
            elif cmd == MpfFile.VALUES:
                datalines = matrixlines[i:]
                break

        # Check if a column legend is duplicated
        for i, legend in enumerate(legends):
            if legends.index(legend)<i:
                msg = 'Duplicate legend "{0}" for column {1} of matrix "{2}" in file "{3}"'.format(
                        legend, i+1, matrixname, self.fname)
                if self.ignore_duplicates:
                    warnings.warn(msg, stacklevel=3)
                else:
                    raise DuplicateKeyError(msg)

        if len(datalines) == 0:
            msg = 'No lines of data in matrix: "{0}"'
            raise ValueError(msg.format(matrixname))

        # Parse the data itself
        values = []
        for line in datalines:
            line = line[MpfFile.COMMAND_LENGTH:]
            linevals = [float(x) for x in line.split()]
            values.append(linevals)
        values = np.asarray(values, dtype=np.float64)
        return matrixname, MpfMatrix(legends, values, attrs=attrs)

    def _getcmd(self, line):
        """
        Get MPF command from a text line, if present.

        :returns: The command and value of an MPF command
        """
        commandstr = line[: MpfFile.COMMAND_LENGTH].strip()
        valuestr = line[MpfFile.COMMAND_LENGTH:].strip()
        return commandstr, valuestr

    # Magic methods, mainly for the immutable sequence protocol

    def __len__(self):
        """
        :returns: The number of matrices in ``self.matrices``.
        """
        return len(self.matrices)

    def __getitem__(self, matrixname):
        """
        :returns: The matrix for a given ``matrixname``.
        """
        return self.matrices[matrixname]

    def __iter__(self):
        """
        :returns: An iterator that iterates over all the matrices in the file.
        """
        return iter(self.matrices)

    def __contains__(self, matrixname):
        """
        Determine whether the matrix ``matrixname`` is found in the file

        :matrixname:    Name of the matrix to check for
        :returns:       True or False, accordingly
        """
        return matrixname in self.matrices

    def __str__(self):
        """
        Convert to readable string object.
        """
        return str(self.matrices)


class DuplicateKeyError(KeyError):
    """
    Key error indicating duplicate keys in an MPF file
    """
    pass


def main():
    sys.exit(0)

if __name__ == '__main__':
    main()

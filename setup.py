#!/usr/bin/env python
# encoding: utf-8

import os
import setuptools

# Read the DESCRIPTION.rst file and insert into the project description
here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'DESCRIPTION.rst')) as f:
    long_description = f.read()

setuptools.setup(
        name='miopyc',
        version='0.3.0.dev1',
        description='Marintek IO PYthon paCkage',
        long_description=long_description,
        url='https://bitbucket.org/marintek/myopic',
        author='Paul Anton Letnes',
        author_email='PaulAnton.Letnes@marintek.sintef.no',
        license='BSD',
        classifiers = [
            'Development Status :: 3 - Alpha',
            'Intended Audience :: Developers',
            'License :: OSI Approved :: BSD License',
            'Programming Language :: Python :: 3.4',
            'Topic :: Scientific/Engineering',
            ],
        keywords='io',
        packages=setuptools.find_packages(),
        install_requires=['numpy>=1.9'],
        package_data={
            # TODO include example files, if necessary
            },
        entry_points={
            'console_scripts': ['mpf2db = miopyc.mpf.mpf2db:main'],
            },
        )

